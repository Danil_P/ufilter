﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;

namespace SegmantationProject.Segmetation
{
    public class SetOfImages
    {
        public Bitmap original;
        public Bitmap current;
        public Bitmap forPixelFilter;
        public Bitmap forBlurFilter;
        public Bitmap forBrightness;
        public Bitmap forBW;
        public Bitmap forInverse;

        public SetOfImages(Bitmap original)
        {
            this.original = original;
            current = new Bitmap(original);
            forPixelFilter = new Bitmap(original);
            forBlurFilter = new Bitmap(original);
            forBrightness = new Bitmap(original);
            forBW = new Bitmap(original);
            forInverse = new Bitmap(original);
        }

        public void ToCurrent(){
            forPixelFilter = new Bitmap(current);
            forBlurFilter = new Bitmap(current);
            forBrightness = new Bitmap(current);
            forBW = new Bitmap(current);
            forInverse = new Bitmap(current);
        }

        public void toCurrentExeptBrightness()
        {
            forPixelFilter = new Bitmap(current);
            forBlurFilter = new Bitmap(current);
            forBW = new Bitmap(current);
            forInverse = new Bitmap(current);
        }

        public void toCurrentExeptPixel()
        {
            forBrightness = new Bitmap(current);
            forBlurFilter = new Bitmap(current);
            forBW = new Bitmap(current);
            forInverse = new Bitmap(current);
        }

        public void toCurrentExeptBlur()
        {
            forBrightness = new Bitmap(current);
            forPixelFilter = new Bitmap(current);
            forBW = new Bitmap(current);
            forInverse = new Bitmap(current);
        }

        public void toCurrentExeptBw()
        {
            forBrightness = new Bitmap(current);
            forPixelFilter = new Bitmap(current);
            forBlurFilter = new Bitmap(current);
            forInverse = new Bitmap(current);
        }

        public void toCurrentExeptInverse()
        {
            forBrightness = new Bitmap(current);
            forPixelFilter = new Bitmap(current);
            forBlurFilter = new Bitmap(current);
            forInverse = new Bitmap(current);
            forBW = new Bitmap(current);
        }

        public void toImageExeptOriginal(Bitmap newBitmap)
        {
            forBrightness = new Bitmap(newBitmap);
            forPixelFilter = new Bitmap(newBitmap);
            forBlurFilter = new Bitmap(newBitmap);
            forBW = new Bitmap(newBitmap);
            current = new Bitmap(newBitmap);
            forInverse = new Bitmap(newBitmap);
        }

        public void toImage(Bitmap newBitmap)
        {
            forBrightness = new Bitmap(newBitmap);
            forPixelFilter = new Bitmap(newBitmap);
            forBlurFilter = new Bitmap(newBitmap);
            forBW = new Bitmap(newBitmap);
            original = new Bitmap(newBitmap);
            current = new Bitmap(newBitmap);
            forInverse = new Bitmap(newBitmap);
        }

    }
}
