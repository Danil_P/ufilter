﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;

namespace SegmantationProject.Segmetation
{
    public class FilterController
    {
        Label name;
        public SuperScrool scroll;
        Label scrollValue;
        Form1 form;

        public FilterController(int positionX, int positionY, Color mainColor, string name, int startValue, Form1 form)
        {

            this.form = form;

            setLabel(positionX, positionY, name, mainColor);
            setScroll(positionX, positionY, mainColor, startValue);
            setScrollValue(positionX, positionY, startValue);

            //Adding
            form.Controls.Add(this.name);
            form.Controls.Add(scroll);
            form.Controls.Add(scrollValue);
        }

        protected void setLabel(int positionX, int positionY, string name, Color mainColor)
        {
            //Creating label with this element's name
            this.name = new Label();
            this.name.Text = name;
            this.name.ForeColor = mainColor;
            this.name.BackColor = form.BackColor;
            this.name.Location = new Point(positionX, positionY);
            this.name.Font = new Font("Arial", 14, FontStyle.Bold);
            this.name.Size = new Size(30, 30);
            this.name.TextAlign = ContentAlignment.MiddleCenter;
        }

        protected void setScroll(int positionX, int positionY, Color mainColor, int startValue)
        {
            //Creating scroll settings
            scroll = new SuperScrool(startValue, 6, mainColor);
            scroll.Location = new Point(positionX + 30, positionY + 12);
            scroll.value = startValue;
            scroll.MouseMove += ScrollValue_MouseMove;
        }

        protected void setScrollValue(int positionX, int positionY, int startValue)
        {
            //Creating TextBox's settings
            scrollValue = new Label();
            scrollValue.Size = new Size(30, 15);
            scrollValue.BackColor = Color.FromArgb(form.BackColor.R + 5, form.BackColor.G + 5, form.BackColor.B + 5);
            scrollValue.BorderStyle = BorderStyle.None;
            scrollValue.ForeColor = Color.Gray;
            scrollValue.Font = new Font("Arial", 9, FontStyle.Regular);
            scrollValue.Location = new Point(positionX + 296, positionY + 5);
            scrollValue.Text = Convert.ToString(scroll.value);
            scrollValue.TextAlign = ContentAlignment.MiddleCenter;
            scrollValue.Text = Convert.ToString(startValue);

        }

        protected void ScrollValue_MouseMove(object sender, MouseEventArgs e)
        {
            scrollValue.Text = Convert.ToString(scroll.value);
        }
    }
}
