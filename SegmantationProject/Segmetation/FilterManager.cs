﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;

namespace SegmantationProject.Segmetation
{
    public class FilterManager
    {
        public Form1 form;
        public SetOfImages imagesSet;
        public FilterManager(Bitmap workImg, Form1 form){
            this.form = form;
            imagesSet = new SetOfImages(workImg);
        }
        

        public Bitmap changeGreen(int valueGreen, int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            BitmapLockManager originalImg = new BitmapLockManager(imagesSet.original);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            originalImg.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();
            for (int i = xPoint; i < xPoint + areaWidth; i++)
            {
                for (int j = yPoint; j < yPoint + areaHeight; j++)
                {
                    if (i < originalImg.Width && i >= 0 && j < originalImg.Height && j >= 0)
                    {
                        int green = originalImg.GetPixel(i, j).G + valueGreen;
                        int red = currentImg.GetPixel(i, j).R;
                        int blue = currentImg.GetPixel(i, j).B;
                        int opacity = currentImg.GetPixel(i, j).A;
                        if (green > 255)
                            green = 255;
                        if (green < 1)
                            green = 0;
                        currentImg.SetPixel(i, j, Color.FromArgb(opacity, red, green, blue));
                    }
                }
            }
            originalImg.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            imagesSet.ToCurrent(); //Приравнием все остальные изображения к текущему
            return imagesSet.current;
        }

        public Bitmap changeRed(int valueRed, int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            BitmapLockManager originalImg = new BitmapLockManager(imagesSet.original);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            originalImg.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();
            for (int i = xPoint; i < xPoint + areaWidth; i++)
            {
                for (int j = yPoint; j < yPoint + areaHeight; j++)
                {
                    if (i < originalImg.Width && i >= 0 && j < originalImg.Height && j >= 0)
                    {
                        int red = originalImg.GetPixel(i, j).R + valueRed;
                        int green = currentImg.GetPixel(i, j).G;
                        int blue = currentImg.GetPixel(i, j).B;
                        int opacity = currentImg.GetPixel(i, j).A;
                        if (red > 255)
                            red = 255;
                        if (red < 1)
                            red = 0;
                        currentImg.SetPixel(i, j, Color.FromArgb(opacity, red, green, blue));
                    }
                }
            }
            originalImg.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            imagesSet.ToCurrent(); //Приравнием все остальные изображения к текущему
            return imagesSet.current;
        }

        public Bitmap changeBlue(int valueBlue, int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            BitmapLockManager originalImg = new BitmapLockManager(imagesSet.original);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            originalImg.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();
            for (int i = xPoint; i < xPoint + areaWidth; i++)
            {
                for (int j = yPoint; j < yPoint + areaHeight; j++)
                {
                    if (i < originalImg.Width && j < originalImg.Height && i>=0 && j >= 0)
                    {
                        int red = currentImg.GetPixel(i, j).R;
                        int green = currentImg.GetPixel(i, j).G;
                        int blue = originalImg.GetPixel(i, j).B + valueBlue;
                        int opacity = currentImg.GetPixel(i, j).A;
                        if (blue > 255)
                            blue = 255;
                        if (blue < 1)
                            blue = 0;
                        currentImg.SetPixel(i, j, Color.FromArgb(opacity, red, green, blue));
                    }
                }
            }
            originalImg.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            imagesSet.ToCurrent(); //Приравнием все остальные изображения к текущему
            return imagesSet.current;
        }

        public Bitmap changeOpacity(int valueOpacity, int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            BitmapLockManager originalImg = new BitmapLockManager(imagesSet.original);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            originalImg.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();
            for (int i = xPoint; i < xPoint + areaWidth; i++)
            {
                for (int j = yPoint; j < yPoint + areaHeight; j++)
                {
                    if (i < originalImg.Width && j < originalImg.Height && i>= 0 && j>= 0)
                    {
                        int opacity = originalImg.GetPixel(i, j).A + valueOpacity;
                        int red = currentImg.GetPixel(i, j).R;
                        int blue = currentImg.GetPixel(i, j).B;
                        int green = currentImg.GetPixel(i, j).G;
                        if (opacity > 255)
                            opacity = 255;
                        if (opacity < 0)
                            opacity = 0;
                        currentImg.SetPixel(i, j, Color.FromArgb(opacity, red, green, blue));
                    }
                }
            }
            originalImg.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            //imagesSet.ToCurrent(imagesSet.forBlurFilter, imagesSet.forPixelFilter); //Приравнием все остальные изображения к текущему
            imagesSet.ToCurrent();
            return imagesSet.current;
        }

        public Bitmap changeBrightness(int valueBright, int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            BitmapLockManager brightnessImg = new BitmapLockManager(imagesSet.forBrightness);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            brightnessImg.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();
            for (int i = xPoint; i < xPoint + areaWidth; i++)
            {
                for (int j = yPoint; j < yPoint + areaHeight; j++)
                {
                    if (i < brightnessImg.Width && j < brightnessImg.Height && i >= 0 && j >= 0)
                    {
                        int red = brightnessImg.GetPixel(i, j).R + valueBright;
                        int green = brightnessImg.GetPixel(i, j).G + valueBright;
                        int blue = brightnessImg.GetPixel(i, j).B + valueBright;
                        int opacity = brightnessImg.GetPixel(i, j).A;
                        if (blue > 255)
                            blue = 255;
                        if (red > 255)
                            red = 255;
                        if (green > 255)
                            green = 255;
                        if (blue < 1)
                            blue = 0;
                        if (red < 1)
                            red = 0;
                        if (green < 1)
                            green = 0;
                        currentImg.SetPixel(i, j, Color.FromArgb(opacity, red, green, blue));
                    }
                }
            }
            brightnessImg.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            //imagesSet.ToCurrent(imagesSet.forBlurFilter, imagesSet.forPixelFilter); //Приравнием все остальные изображения к текущему
            imagesSet.toCurrentExeptBrightness();
            return imagesSet.current; 
        }

        public Bitmap makePixelImage(int valuePixelRadius, int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            if (valuePixelRadius < 1)
            {
                imagesSet.current = new Bitmap(imagesSet.forPixelFilter);
                imagesSet.toCurrentExeptPixel();
                return imagesSet.current;
            }

            BitmapLockManager pixelFilter = new BitmapLockManager(imagesSet.forPixelFilter);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            pixelFilter.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();

            for (int i = xPoint; i < areaWidth + xPoint; i += valuePixelRadius)
            {
             
                for (int j = yPoint; j < areaHeight + yPoint; j += valuePixelRadius)
                {

                    int red = 0;
                    int green = 0;
                    int blue = 0;
                    int opacity;
                    int devider = 0;
                    if (i < pixelFilter.Width && j < pixelFilter.Height && i > -1 && j>-1)
                        opacity = pixelFilter.GetPixel(i, j).A;
                    else
                        opacity = 100;
                    for (int i1 = i; i1 < i + valuePixelRadius; i1++)
                    {
                        for (int j1 = j; j1 < j + valuePixelRadius; j1++)
                        {
                            if (i1 < pixelFilter.Width && j1 < pixelFilter.Height && i1 >= 0 && j1 >= 0)
                            {
                                devider++;
                                red += pixelFilter.GetPixel(i1, j1).R;
                                green += pixelFilter.GetPixel(i1, j1).G;
                                blue += pixelFilter.GetPixel(i1, j1).B;
                            }
                        }
                    }

                    for (int i1 = i; i1 < i + valuePixelRadius; i1++)
                    {
                        for (int j1 = j; j1 < j + valuePixelRadius; j1++)
                        {
                            if (i1 < pixelFilter.Width && j1 < pixelFilter.Height && i1 >= 0 && j1 >= 0)
                            {
                                currentImg.SetPixel(i1, j1, Color.FromArgb(opacity, red / devider, green / devider, blue / devider));
                            }
                        }
                    }
                }
            }

            pixelFilter.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            //imagesSet.ToCurrent(imagesSet.forBlurFilter, imagesSet.forPixelFilter); //Приравнием все остальные изображения к текущему
            imagesSet.toCurrentExeptPixel();
            return imagesSet.current;
        }

        public Bitmap makeBlur(int valueBlurRadius, int areaWidth, int areaHeight)
        {
            if (valueBlurRadius < 1)
            {
                imagesSet.current = new Bitmap(imagesSet.forBlurFilter);
                imagesSet.toCurrentExeptBlur();
                return imagesSet.current;
            }

            BitmapLockManager blurFilter = new BitmapLockManager(imagesSet.forBlurFilter);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            blurFilter.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();

            for (int i = 0; i < areaWidth; i+=2 )
            {

                for (int j = 0; j < areaHeight; j+=2)
                {

                    int red = 0;
                    int green = 0;
                    int blue = 0;
                    int opacity = blurFilter.GetPixel(i, j).A;
                    for (int i1 = i; i1 < i + valueBlurRadius; i1++)
                    {
                        for (int j1 = j; j1 < j + valueBlurRadius; j1++)
                        {
                            if (i1 < areaWidth && j1 < areaHeight)
                            {
                                red += blurFilter.GetPixel(i1, j1).R / valueBlurRadius;
                                green += blurFilter.GetPixel(i1, j1).G / valueBlurRadius;
                                blue += blurFilter.GetPixel(i1, j1).B / valueBlurRadius;
                            }
                        }
                    }

                    for (int i1 = i; i1 < i + valueBlurRadius; i1++)
                    {
                        for (int j1 = j; j1 < j + valueBlurRadius; j1++)
                        {
                            if (i1 < areaWidth && j1 < areaHeight)
                            {
                                currentImg.SetPixel(i1, j1, Color.FromArgb(opacity, red / valueBlurRadius, green / valueBlurRadius, blue / valueBlurRadius));
                            }
                        }
                    }
                }
            }

            blurFilter.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            imagesSet.toCurrentExeptBlur();

            return imagesSet.current;
        }

        //For BUTTON
        /*public Bitmap makeBlackAndWhite(int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            BitmapLockManager originalImg = new BitmapLockManager(imagesSet.forBW);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            originalImg.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();

                for (int i = xPoint; i < xPoint + areaWidth; i++)
                {
                    for (int j = yPoint; j < yPoint + areaHeight; j++)
                    {
                        if (i < originalImg.Width && j < originalImg.Height && i > 0 && j > 0)
                        {
                            int red = currentImg.GetPixel(i, j).R;
                            int blue = currentImg.GetPixel(i, j).B;
                            int green = currentImg.GetPixel(i, j).G;
                            int average = (green + blue + red) / 3;
                            currentImg.SetPixel(i, j, Color.FromArgb(average, average, average));
                        }
                    }
                }

            originalImg.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            imagesSet.toCurrentExeptBw(); //Приравнием все остальные изображения к текущему
            return imagesSet.current;
        }*/

        public Bitmap Inverse(int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            BitmapLockManager originalImg = new BitmapLockManager(imagesSet.forInverse);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            originalImg.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();

            for (int i = xPoint; i < areaWidth + xPoint; i++)
            {
                for (int j = yPoint; j < areaHeight + yPoint; j++)
                {
                    if (i < originalImg.Width && i >= 0 && j < originalImg.Height && j >= 0)
                    {
                        int red = 255 - currentImg.GetPixel(i, j).R;
                        int blue = 255 - currentImg.GetPixel(i, j).B;
                        int green = 255 - currentImg.GetPixel(i, j).G;
                        currentImg.SetPixel(i, j, Color.FromArgb(red, green, blue));
                    }
                }
            }

            originalImg.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            imagesSet.toCurrentExeptInverse(); //Приравнием все остальные изображения к текущему
            return imagesSet.current;
        }

        public Bitmap makeBlackAndWhiteScroll(double bwValue, int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            BitmapLockManager originalImg = new BitmapLockManager(imagesSet.forBW);
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            originalImg.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый цвет
            currentImg.LockBits();

            if (bwValue < 1) bwValue = 1;
            if (bwValue > 100) bwValue = 100;

            double m = bwValue / 100;

            for (int i = xPoint; i < xPoint + areaWidth; i++)
            {
                for (int j = yPoint; j < yPoint + areaHeight; j++)
                {
                    if (i < originalImg.Width && i >= 0 && j < originalImg.Height && j >= 0)
                    {
                        Color c = originalImg.GetPixel(i, j);
                        int averageColorValue = (c.B + c.G + c.R) / 3;
                        int n1 = (int)((c.B - averageColorValue) * m);
                        int n2 = (int)((c.G - averageColorValue) * m);
                        int n3 = (int)((c.R - averageColorValue) * m);
                        currentImg.SetPixel(i, j, Color.FromArgb(c.R - n3, c.G - n2, c.B - n1));
                    }
                }
            }

            originalImg.UnlockBits();
            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            imagesSet.toCurrentExeptBw(); //Приравнием все остальные изображения к текущему
            return imagesSet.current;
        }




        public Bitmap makeMonochrome(int xPoint, int yPoint, int areaWidth, int areaHeight)
        {
            BitmapLockManager currentImg = new BitmapLockManager(imagesSet.current);
            currentImg.LockBits();

            for (int i = xPoint; i < areaWidth + xPoint; i++)
            {
                for (int j = yPoint; j < areaHeight + yPoint; j++)
                {
                    if (i < currentImg.Width && i >= 0 && j < currentImg.Height && j >= 0)
                    {
                        int red = currentImg.GetPixel(i, j).R;
                        int blue = currentImg.GetPixel(i, j).B;
                        int green = currentImg.GetPixel(i, j).G;
                        if((red+green+blue)/3 >= 128)
                            currentImg.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                        else
                            currentImg.SetPixel(i, j, Color.FromArgb(255, 255, 255));
                    }
                }
            }

            currentImg.UnlockBits();
            imagesSet.current = currentImg.GetImage(); //Тут получаем обработанное изображение с класса BitmapLockManager
            imagesSet.ToCurrent(); //Приравнием все остальные изображения к текущему
            return imagesSet.current;
        }



    }
}
