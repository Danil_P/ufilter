﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;
namespace SegmantationProject.Segmetation
{
    public class FunctionButtonForTopMenu : Button
    {
        public FunctionButtonForTopMenu(string text)
        {
            this.FlatStyle = FlatStyle.Flat;
            this.BackColor = Color.Black;
            this.FlatAppearance.BorderSize = 0;
            this.FlatAppearance.MouseOverBackColor = Color.Black;
            this.FlatAppearance.MouseDownBackColor = Color.DarkOrange;
            this.Text = text;
            this.Font = new Font("Arial", 10, FontStyle.Regular);
            this.TextAlign = ContentAlignment.MiddleCenter;
            this.ForeColor = Color.FromArgb(220, 218, 218);
            this.Size = new Size(53, 30);
            this.BackColor = Color.FromArgb(255, Color.FromArgb(20,18,18));
        }

    }
}
