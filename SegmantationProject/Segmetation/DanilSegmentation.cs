﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;

namespace SegmantationProject.Segmetation
{   
    class PixelColor
    {
        public int red, green, blue;
        public PixelColor(int red, int green, int blue)
        {
            this.red = red;
            this.blue = blue;
            this.green = green;
        }
    }

    class DanilSegmentation
    {
        Color[] coll = { Color.Red, Color.Blue, Color.Plum, Color.Silver, Color.YellowGreen, Color.PapayaWhip,
                         Color.SpringGreen, Color.Tan, Color.Black, Color.Red, Color.Plum,
                        Color.Silver, Color.YellowGreen, Color.PapayaWhip,
                         Color.SpringGreen, Color.Tan, Color.Black};
        Bitmap mainImage;
        PixelColor[,] imagePixelColor;
        int numberOfSegments;
        int[,] whichSegment;
        bool[,] used;
        public DanilSegmentation(PictureBox mainPictureBox)
        {
            mainImage = new Bitmap(mainPictureBox.Image);
            imagePixelColor = new PixelColor[mainImage.Width, mainImage.Height];
            whichSegment = new int[mainImage.Width, mainImage.Height];
            used = new bool[mainImage.Width, mainImage.Height];
            numberOfSegments = 1;
            imageToMatrix();
            setWhichSegment();
        }



        public void imageToMatrix()
        {
            BitmapLockManager originalImg = new BitmapLockManager(mainImage);
            originalImg.LockBits();     //Открываем оригинальное изображение, чтобы взять оттуда изменяемый 
            for (int i = 0; i < mainImage.Width; i++)
                for (int j = 0; j < mainImage.Height; j++)
                {
                    Color pixelColor = originalImg.GetPixel(i, j);
                    int red = pixelColor.R;
                    int green = pixelColor.G;
                    int blue = pixelColor.B;
                    imagePixelColor[i,j] = new PixelColor(red, green, blue);
                }

            originalImg.UnlockBits();
        }

        void setWhichSegment()
        {
            for(int i = 0; i <  mainImage.Width; i++)
            {
                for (int j = 0; j < mainImage.Height; j++)
                {
                    whichSegment[i, j] = -1;
                    used[i,j] = false;
                }
            }
        }

        public void deepSearch(int segmentationValue, int x, int y)
        {
            if (used[x, y] || !cellExist(x, y))
                return;
            whichSegment[x,y] = numberOfSegments;
            used[x, y] = true;

            if(cellExist(x + 1, y) && !used[x+1, y])
                if(calculateDistance(x, y, x+1, y) < segmentationValue)
                    deepSearch(segmentationValue, x+1, y);

           /* if (cellExist(x - 1, y) && !used[x-1, y] )
                if (calculateDistance(x, y, x - 1, y) < segmentationValue)
                    deepSearch(segmentationValue, x - 1, y);*/

            if (cellExist(x, y+1) && !used[x, y+1])
                if (calculateDistance(x, y, x, y+1) < segmentationValue)
                    deepSearch(segmentationValue, x, y+1);

            if (cellExist(x, y-1) && !used[x, y-1])
                if (calculateDistance(x, y, x, y-1) < segmentationValue)
                    deepSearch(segmentationValue, x, y-1);

            return;
        }



        public Bitmap makeSegmentation()
        {
            for (int i = 0; i < mainImage.Width; i++)
                for (int j = 0; j < mainImage.Height; j++)
                {
                    if (!used[i, j])
                    {
                        deepSearch(numberOfSegments, i, j);
                        numberOfSegments++;
                    }
                }

            for (int i = 0; i<mainImage.Width; i++)
                for(int j = 0; j<mainImage.Height; j++)
                {
                    if (whichSegment[i, j] <= numberOfSegments)
                    {
                        int red = whichSegment[i,j] * 10;
                        int blue = whichSegment[i, j]*3;
                        int green = whichSegment[i, j]* whichSegment[i, j]+50 - whichSegment[i,j]*2;
                        if (red > 255) red = 255;
                        if (blue > 255) blue = 255;
                        if (green > 255) green = 255;
                        if (red < 0) red = 0;
                        if (green < 0) green = 0;
                        if (blue < 0) blue = 0;
                        mainImage.SetPixel(i, j, Color.FromArgb(red, blue, green));
                    }
                }

            return mainImage;
        }


        bool cellExist(int x, int y)
        {
            if (x < 0 || x >= mainImage.Width || y < 0 || y >= mainImage.Height)
                return false;
            return true;
        }

        int calculateDistance(int x, int y, int x1, int y1)
        {
            return Convert.ToInt16(Math.Sqrt(Math.Pow((imagePixelColor[x, y].red - imagePixelColor[x1, y1].red), 2) + Math.Pow((imagePixelColor[x, y].green - imagePixelColor[x1, y1].green), 2) + Math.Pow((imagePixelColor[x, y].blue - imagePixelColor[x1, y1].blue), 2)));
        }

    }
}