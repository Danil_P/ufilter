﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;

namespace SegmantationProject.Segmetation
{
    class TopMenu
    {
        PictureBox background;
        Form1 form;
        PictureBox mainPicture;
        public TopMenu(Form1 form, PictureBox mainPicture)
        {
            this.form = form;
            this.mainPicture = mainPicture;
            setLogo();
            setMenu();
            setBackground();
        }

        void setBackground()
        {
            background = new PictureBox();
            background.Size = new Size(form.Width, 30);
            background.Location = new Point(0, 0);
            background.BackColor = Color.FromArgb(20, 18, 18);
            background.SendToBack();
            form.Controls.Add(background);
        }

        void setMenu()
        {
            FunctionButtonForTopMenu open = new FunctionButtonForTopMenu("Open");
            FunctionButtonForTopMenu save = new FunctionButtonForTopMenu("Save");
            FunctionButtonForTopMenu exit = new FunctionButtonForTopMenu("Exit");
            FunctionButtonForTopMenu rollUp = new FunctionButtonForTopMenu(" _");

            open.Location = new Point(50, 0);
            open.BringToFront();
            open.Click += Open_Click;
            form.Controls.Add(open);

            save.Location = new Point(110, 0);
            save.BringToFront();
            save.Click += Save_Click;
            form.Controls.Add(save);

            exit.Location = new Point(170, 0);
            exit.Click += Exit_Click;
            exit.BringToFront();
            form.Controls.Add(exit);

            rollUp.Location = new Point(form.Width - 54, 0);
            rollUp.Click += rollUp_Click;
            rollUp.BringToFront();
            form.Controls.Add(rollUp);
        }

        private void rollUp_Click(object sender, EventArgs e)
        {
            form.WindowState = FormWindowState.Minimized;
        }

        private void Open_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = "c:";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    mainPicture.LoadAsync(ofd.FileName);
                }
                catch (Exception)
                {
                    MessageBox.Show("Can't open this file");
                }
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.InitialDirectory = "c:";
            fileDialog.CheckPathExists = true;
            fileDialog.OverwritePrompt = true;
            if(fileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    mainPicture.Image.Save(fileDialog.FileName, System.Drawing.Imaging.ImageFormat.Png);
                }
                catch (Exception)
                {
                    MessageBox.Show("Can't save an image", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }           

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Exit from the app?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                form.Close();
        }

        void setLogo()
        {
            Label logo = new Label();
            logo.Text = "UF |";
            logo.Font = new Font("Arial", 13, FontStyle.Bold);
            logo.ForeColor = Color.FromArgb(220, 82, 218);
            logo.Location = new Point(4, 4);
            logo.Size = new Size(44, 20);
            logo.BackColor = Color.FromArgb(20, 18, 18);
            logo.BringToFront();
            form.Controls.Add(logo);
        }
    }
}
