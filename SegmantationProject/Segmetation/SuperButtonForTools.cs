﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;

namespace SegmantationProject.Segmetation
{
    class SuperButtonForTools: Button
    {
        int clickCounter;
        Color mainColor;
        public SuperButtonForTools(Color buttonColor, Color textColor, string text)
        {
            this.mainColor = buttonColor;
            clickCounter = 0;
            this.Size = new Size(54, 25);
            this.BackColor = Color.FromArgb(46, 45, 45);
            this.FlatStyle = FlatStyle.Flat;
            this.Text = text;
            this.MouseClick += SuperButton_MouseClick;
            this.ForeColor = textColor;
            this.FlatAppearance.BorderSize = 0;
            this.Font = new Font("Times new roman", 7, FontStyle.Bold);
        }

        private void SuperButton_MouseClick(object sender, MouseEventArgs e)
        {
            if (clickCounter % 2 == 0)
            {
                this.BackColor = mainColor;
            }
            else
            {
                this.BackColor = Color.FromArgb(46, 45, 45);
            }
            clickCounter++;
        }

        public int getClickCounter()
        {
            return clickCounter;
        }
    }
}
