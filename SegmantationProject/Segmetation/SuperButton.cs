﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;
namespace SegmantationProject.Segmetation
{
    public class SuperButton:Button
    {
        Color mainColor;
        public SuperButton(Color buttonColor, Color textColor, string text)
        {
            this.mainColor = buttonColor;
            this.Size = new Size(54, 25);
            this.BackColor = Color.FromArgb(46,45,45);
            this.FlatStyle = FlatStyle.Flat;
            this.Text = text;
            this.ForeColor = textColor;
            this.FlatAppearance.BorderSize = 0;
            this.Font = new Font("Times new roman", 7, FontStyle.Bold);
        }

    }
}
