﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using SegmantationProject.Segmetation;

namespace SegmantationProject.Segmetation
{


    class Pair
    {
        public int x;
        public int y;
        public Pair(int _first, int _second)
        {
            x = _first;
            y = _second;
        }
    }

    public class TwoDimTools
    {
        public bool[,] u;
        public int[,] a;
        public int[] dx;
        public int[] dy;
        public List<int> r;
        public List<int> g;
        public List<int> b;
        public TwoDimTools(int wd, int ht)
        {
            u = new bool[wd, ht];
            a = new int[wd, ht];
            dx = new int[] { -1,  0, 1, 0, -1, -1,  1, 1 };
            dy = new int[] {  0, -1, 0, 1, -1,  1, -1, 1 };
            r = new List<int>();
            g = new List<int>();
            b = new List<int>();
            r.Add(0);
            g.Add(0);
            b.Add(0);
        }
        public bool check(int x, int y, int x2, int y2)
        {
            return (x >= 0 && x < x2 && y >= 0 && y < y2);
        }

        public bool chclr(int cur, int sz, int nw)
        {
            return (cur - sz <= nw && nw <= cur + sz);
        }

        public void run(int c, int x, int y, int th, Bitmap bmp)
        {
            u[x, y] = true;
            a[x, y] = c;
            Queue<Pair> q = new Queue<Pair>();
            List<int> rt = new List<int>();
            List<int> gt = new List<int>();
            List<int> bt = new List<int>();
            int w = bmp.Width, h = bmp.Height, count = 1;
            q.Enqueue(new Pair(x, y));
            Color color = bmp.GetPixel(x, y);
            int red = color.R, green = color.G, blue = color.B;
            rt.Add(color.R);
            gt.Add(color.G);
            bt.Add(color.B);
            while (q.Count != 0)
            {
                Pair p = q.Peek();
                Color cur = bmp.GetPixel(p.x, p.y);
                q.Dequeue();
                for (int i = 0; i < 8; ++i)
                {
                    int xx = p.x + dx[i], yy = p.y + dy[i];
                    if (check(xx, yy, w, h) && !u[xx, yy])
                    {
                        Color nwc = bmp.GetPixel(xx, yy);
                        if (chclr(blue, th, nwc.B) && chclr(green, th, nwc.G) && chclr(red, th, nwc.R))
                        {
                            u[xx, yy] = true;
                            a[xx, yy] = c;
                            q.Enqueue(new Pair(xx, yy));
                            count++;
                            rt.Add(nwc.R);
                            gt.Add(nwc.G);
                            bt.Add(nwc.B);
                        }
                    }
                }
            }
            int rall = 0, gall = 0, ball = 0;
            for (int i = 0; i < rt.Count; ++i)
            {
                rall += rt[i];
                gall += gt[i];
                ball += bt[i];
            }
            int nred = rall / count, ngreen = gall / count, nblue = ball / count;
            r.Add(nred);
            g.Add(ngreen);
            b.Add(nblue);
        }
        public Bitmap s2d(Bitmap bmp, int sz)
        {
            Bitmap res = new Bitmap(bmp.Width, bmp.Height);
            int wd = bmp.Width, ht = bmp.Height, c = 1;
            for (int x = 0; x < wd; ++x)
            {
                for (int y = 0; y < ht; ++y)
                {
                    if (!u[x, y])
                    {
                        run(c++, x, y, sz, bmp);
                    }
                }
            }
            for (int x = 0; x < wd; ++x)
            {
                for (int y = 0; y < ht; ++y)
                {
                    res.SetPixel(x, y, Color.FromArgb(r[a[x, y]], g[a[x, y]], b[a[x, y]]));
                }
            }
            return res;
        }
    }
}
