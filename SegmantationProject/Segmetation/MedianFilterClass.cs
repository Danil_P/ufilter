﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using SegmantationProject.Segmetation;

namespace SegmantationProject.Segmetation
{

    class Pair1
    {
        public double f;
        public Px s;
        public Pair1(double _first, Px _second)
        {
            f = _first;
            s = _second;
        }
    }

    class Px
    {
        public int b;
        public int g;
        public int r;
        public Px(int _b, int _g, int _r)
        {
            b = _b;
            g = _g;
            r = _r;
        }
        public double val()
        {
            double v = Math.Sqrt(b * b + g * g + r * r);
            return v;
        }
    }


    class MedianFilterClass
    {
        public void sort(List<Pair1> a, int left, int right)
        {
            int l = left, r = right;
            double m = a[(l + r) / 2].f;
            while (l <= r)
            {
                while (a[l].f < m)
                    l++;
                while (a[r].f > m)
                    r--;
                if (l <= r)
                {
                    if (a[l].f > a[r].f)
                    {
                        Pair1 t = a[r];
                        a[r] = a[l];
                        a[l] = t;
                    }
                    l++;
                    r--;
                }
            }
            if (l < right)
                sort(a, l, right);
            if (left < r)
                sort(a, left, r);
        }

        public Bitmap MedFilter(Bitmap bmp, int sz)
        {
            Bitmap res = new Bitmap(bmp.Width, bmp.Height);
            if (sz > 2)
            {
                if (sz % 2 == 0)
                    sz -= 1;
                BitmapLockManager original = new BitmapLockManager(bmp);
                BitmapLockManager current = new BitmapLockManager(res);
                original.LockBits();
                current.LockBits();
                int wd = res.Width, ht = res.Height;
                List<Pair1> l = new List<Pair1>();
                for (int xx = 0; xx < wd - sz; ++xx)
                {
                    for (int yy = 0; yy < ht - sz; ++yy)
                    {
                        for (int x = xx; (x < xx + sz && x < wd); ++x)
                        {
                            for (int y = yy; (y < yy + sz && y < ht); ++y)
                            {
                                Color pixel = original.GetPixel(x, y);
                                Px p = new Px(pixel.B, pixel.G, pixel.R);
                                l.Add(new Pair1(p.val(), p));
                            }
                        }
                        sort(l, 0, l.Count - 1);
                        Px av = l[l.Count / 2].s;
                        l.Clear();
                        for (int x = xx; x < xx + sz && x < wd; ++x)
                            for (int y = yy; y < yy + sz && y < ht; ++y)
                                current.SetPixel(x, y, Color.FromArgb(av.r, av.g, av.b));
                    }
                }
                original.UnlockBits();
                current.UnlockBits();
                return current.GetImage();
            }
            return bmp;
        }
    }
}
