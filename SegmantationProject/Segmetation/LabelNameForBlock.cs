﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;

namespace SegmantationProject.Segmetation
{
    public class LabelNameForBlock:Label
    {
        public LabelNameForBlock(string s)
        {
            this.Text = s;
            this.Font = new Font("Arial", 9, FontStyle.Bold);
            this.ForeColor = Color.FromArgb(181, 181, 181);
            this.TextAlign = ContentAlignment.MiddleCenter;
            this.Size = new Size(316, 23);
        }
    }
}
