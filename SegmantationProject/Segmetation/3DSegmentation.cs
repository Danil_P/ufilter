﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace SegmantationProject.Segmetation
{
    class Triple
    {
        public int x, y, z;
        public Triple(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }


    public class GetImg
    {
        public Bitmap[] b;
        public GetImg(string[] s)
        {
            b = new Bitmap[s.Length];
            for (int i = 0; i < s.Length; ++i)
            {
                b[i] = new Bitmap(s[i]);
            }
        }
        public GetImg(int x, int y, int z)
        {
            b = new Bitmap[z];
            for (int i = 0; i < z; ++i)
                b[i] = new Bitmap(x, y);
        }
    }

    public class Segmentation3D
    {
        public bool[,,] u;
        public int[,,] a;
        public int[] dx;
        public int[] dy;
        public int[] dz;
        public List<int> r;
        public List<int> g;
        public List<int> b;
        public Bitmap[] td;
        public Segmentation3D(int wd, int ht, int ln)
        {
            u = new bool[wd, ht, ln];
            a = new int[wd, ht, ln];
            td = new Bitmap[ln];
            dx = new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1,  0,  0,  0,  0, 0,  0, 0, 0,  1,  1,  1,  1, 1, 1,  1, 1, 1 };
            dy = new int[] { -1, -1, -1,  0,  0,  0,  1,  1,  1, -1, -1, -1,  0, 0,  1, 1, 1, -1, -1, -1,  0, 0, 0,  1, 1, 1 };
            dz = new int[] { -1,  0,  1, -1,  0,  1, -1,  0,  1, -1,  0,  1, -1, 1, -1, 0, 1, -1,  0,  1, -1, 0, 1, -1, 0, 1 };
            r = new List<int>();
            g = new List<int>();
            b = new List<int>();
            r.Add(0);
            g.Add(0);
            b.Add(0);
        }
        public bool check(int x, int y, int z, int x2, int y2, int z2)
        {
            return (x >= 0 && x < x2 && y >= 0 && y < y2 && z >= 0 && z < z2);
        }
        public bool chclr(int cur, int sz, int nw)
        {
            return (cur - sz <= nw && nw <= cur + sz);
        }
        public void run(int c, int x, int y, int z, int th, GetImg gi)
        {
            u[x, y, z] = true;
            a[x, y, z] = c;
            Queue<Triple> q = new Queue<Triple>();
            List<int> rt = new List<int>();
            List<int> gt = new List<int>();
            List<int> bt = new List<int>();
            int w = gi.b[z].Width, h = gi.b[z].Height, l = gi.b.Length, count = 1;
            q.Enqueue(new Triple(x, y, z));
            Color color = gi.b[z].GetPixel(x, y);
            int red = color.R, green = color.G, blue = color.B;
            rt.Add(color.R);
            gt.Add(color.G);
            bt.Add(color.B);
            while (q.Count != 0)
            {
                Triple t = q.Peek();
                q.Dequeue();
                Color cur = gi.b[t.z].GetPixel(t.x, t.y);
                for (int i = 0; i < 26; ++i)
                {
                    int xx = t.x + dx[i], yy = t.y + dy[i], zz = t.z + dz[i];
                    if (check(xx, yy, zz, w, h, l) && !u[xx, yy, zz])
                    {
                        Color nwc = gi.b[zz].GetPixel(xx, yy);
                        if (chclr(blue, th, nwc.B) && chclr(green, th, nwc.G) && chclr(red, th, nwc.R))
                        {
                            u[xx, yy, zz] = true;
                            a[xx, yy, zz] = c;
                            q.Enqueue(new Triple(xx, yy, zz));
                            count++;
                            rt.Add(nwc.R);
                            gt.Add(nwc.G);
                            bt.Add(nwc.B);
                        }
                    }
                }
            }
            int rall = 0, gall = 0, ball = 0;
            for (int i = 0; i < rt.Count; ++i)
            {
                rall += rt[i];
                gall += gt[i];
                ball += bt[i];
            }
            int nred = rall / count, ngreen = gall / count, nblue = ball / count;
            r.Add(nred);
            g.Add(ngreen);
            b.Add(nblue);
        }
        public GetImg s3d(GetImg gi, int th, int wd, int ht, int ln)
        {
            GetImg res = new GetImg(wd, ht, ln);
            int c = 1;
            for (int z = 0; z < ln; ++z)
            {
                for (int x = 0; x < wd; ++x)
                {
                    for (int y = 0; y < ht; ++y)
                    {
                        if (!u[x, y, z])
                        {
                            run(c++, x, y, z, th, gi);
                        }
                    }
                }
            }
            for (int z = 0; z < ln; ++z)
            {
                for (int x = 0; x < wd; ++x)
                {
                    for (int y = 0; y < ht; ++y)
                    {
                        res.b[z].SetPixel(x, y, Color.FromArgb(r[a[x, y, z]], g[a[x, y, z]], b[a[x, y, z]]));
                    }
                }
            }
            return res;
        }
        public GetImg mono3d(GetImg gi, int th, int wd, int ht, int ln)
        {
            GetImg res = new GetImg(wd, ht, ln);
            for (int z = 0; z < ln; ++z)
            {
                for (int x = 0; x < wd; ++x)
                {
                    for (int y = 0; y < ht; ++y)
                    {
                        Color c = gi.b[z].GetPixel(x, y);
                        int av = (c.R + c.G + c.B) / 3;
                        res.b[z].SetPixel(x, y, (av <= th ? Color.Black : Color.White));
                    }
                }
            }
            return res;
        }
    }
}
