﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
namespace SegmantationProject.Segmetation
{
    public class ActiveArea
    {
        PictureBox mainPicture;
        bool isPressed;
        Bitmap original, current;
        public bool enable;
        int currentX, currentY, pressedX, pressedY;
        public int originalX, originalY, originalWidth, originalHeihgt;
        public ActiveArea(PictureBox mainPicture)
        {
            enable = false;
            isPressed = false;
            this.mainPicture = mainPicture;
            original = (Bitmap)mainPicture.Image;
            mainPicture.MouseDown += MainPicture_MouseDown;
            mainPicture.MouseUp += MainPicture_MouseUp;
            mainPicture.MouseMove += MainPicture_MouseMove;
        }

        private void MainPicture_MouseMove(object sender, MouseEventArgs e)
        {
            if (enable && isPressed)
            {
                currentX = e.X /*- 60*/;
                currentY = e.Y /*- 80*/;
                Graphics g = Graphics.FromImage(current);
                Pen shonPen = new Pen(Color.DarkSlateBlue, 3);
                g.DrawRectangle(shonPen, Math.Min(currentX, pressedX), Math.Min(currentY, pressedY), Math.Abs(currentX - pressedX), Math.Abs(currentY - pressedY));
                mainPicture.Image = current;
            }
        }

        private void MainPicture_MouseUp(object sender, MouseEventArgs e)
        {
            if (enable)
            {
                isPressed = false;
                mainPicture.Image = original;
                current = new Bitmap(original);
                originalX = Math.Min(currentX, pressedX);
                originalY = Math.Min(currentY, pressedY);
                originalWidth = Math.Abs(currentX - pressedX);
                originalHeihgt = Math.Abs(currentY - pressedY);
            }
        }

        private void MainPicture_MouseDown(object sender, MouseEventArgs e)
        {
            if (enable)
            {
                original = (Bitmap)mainPicture.Image;
                current = new Bitmap(mainPicture.Image);
                pressedX = e.X /*- 60*/;
                pressedY = e.Y /*- 80*/;
                isPressed = true;
            }
        }

        public void changeMainImage(PictureBox newImage)
        {
            mainPicture.Image = newImage.Image;
            original = new Bitmap(mainPicture.Image);
        }
    }
}