﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace classSuperScroolBox.superScrool
{
    public class SuperScrool : PictureBox
    {
        bool pressedButton = false;
        int mouseX;
        int currentX;
        public int value;
        int stringWidth;
        Color mainColor;
        
        public SuperScrool(int currentX, int stringWidth, Color mainColor) {
            Size = new Size(256, 8);
            this.stringWidth = stringWidth;
            this.currentX = currentX;
            this.mainColor = mainColor;

            Bitmap b = new Bitmap(256, stringWidth);
            Graphics g = Graphics.FromImage(b);
            g.DrawLine(new Pen(mainColor, stringWidth), 0, 0, currentX, 0);
            g.DrawLine(new Pen(Color.FromArgb(17, 17, 17), stringWidth), currentX, 0, 256, 0);
            Image = b;

            this.MouseDown += SuperScrool_MouseDown;
            this.MouseUp += SuperScrool_MouseUp;
            this.MouseMove += SuperScrool_MouseMove;
        }

        private void SuperScrool_MouseMove(object sender, MouseEventArgs e)
        {
            if (pressedButton)
            {
                Bitmap b = new Bitmap(Image);
                Graphics g = Graphics.FromImage(b);
                g.Clear(Color.FromArgb(32, 32, 30));
                g.DrawLine(new Pen(mainColor, stringWidth), 0, 0, e.X, 0);
                g.DrawLine(new Pen(Color.FromArgb(17, 17, 17), stringWidth), e.X, 0, 256, 0);
                Image = b;
                g.DrawLine(new Pen(Color.FromArgb(value, 255 - value, 46 ), stringWidth), currentX, 0, e.X, 0);
                Image = b;
                mouseX = e.X;
                if (e.X > 255)
                    value = 255;
                else
                    if (e.X < 0)
                        value = 0;
                    else
                        value = e.X;
            }
        }

        private void SuperScrool_MouseUp(object sender, MouseEventArgs e)
        {
            Bitmap b = new Bitmap(256, stringWidth);
            Graphics g = Graphics.FromImage(b);
            g.DrawLine(new Pen(mainColor, stringWidth), 0, 0, e.X, 0);
            g.DrawLine(new Pen(Color.FromArgb(17, 17, 17), stringWidth), e.X, 0, 256, 0);
            Image = b;
            currentX = e.X;
            pressedButton = false;
            if (currentX > 255)
            {
                value = 255;
            }
            else
                if (currentX < 0)
            {
                value = 0;
            }
            else
            {
                value = currentX;
            }
        }

        private void SuperScrool_MouseDown(object sender, MouseEventArgs e)
        {
            pressedButton = true;
        }

    }
}
