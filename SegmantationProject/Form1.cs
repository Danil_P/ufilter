﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SegmantationProject.Segmetation;
using System.IO;


namespace SegmantationProject
{
    public partial class Form1 : Form
    {
        DanilSegmentation ds;
        PictureBox pb;
        Bitmap b;
        FilterManager fm;
        FilterController greenScroll;
        FilterController redScroll;
        FilterController blueScroll;
        FilterController brightnessScroll;
        FilterController pixelScroll;
        FilterController opacityScroll;
        FilterController blackWhiteScroll;
        FilterController segmentation2DScroll;
        //SuperButton blackWhiteButton;
        SuperButtonForTools inverseButton;
        SuperButton segmentationButton;
        SuperButtonForTools activeAreaButton;
        SuperButton smoothButton;
        SuperButton monoChrome;
        ActiveArea activeArea;
        MedianFilterClass medianFilter;


        public Form1()
        {
            //Creating interface
            InitializeComponent();
            medianFilter = new MedianFilterClass();

            this.Icon = new Icon("favicon.ico");
            this.BackColor = Color.FromArgb(32, 30, 30);
            this.FormBorderStyle = FormBorderStyle.None;
            this.TopMost = true;
            this.Bounds = Screen.PrimaryScreen.Bounds;
            b = new Bitmap("Pet-cat-toilet-2.jpg");
            pb = new PictureBox();
            pb.WaitOnLoad = false;
            pb.LoadAsync("Pet-cat-toilet-2.jpg");
            TopMenu topMenu = new TopMenu(this, pb);
            pb.LoadCompleted += Pb_LoadCompleted;

            pb.BackgroundImage = new Bitmap("backgroundPattern.png");
            pb.Size = new Size(800, 600);
            pb.SizeMode = PictureBoxSizeMode.Zoom;
            pb.Location = new Point(60, 80);
            pb.Image = b;
            
            PictureBox imageBorder = new PictureBox();
            imageBorder.Location = new Point(51, 72);
            imageBorder.Size = new Size(816, 618);
            imageBorder.Image = new Bitmap("imageBorder.png");
            
            this.Controls.Add(imageBorder); 
            this.Controls.Add(pb);

            fm = new FilterManager(b, this);
            imageBorder.SendToBack();

            //Creating color scrolls block
            LabelNameForBlock colorName = new LabelNameForBlock("Work with color");
            colorName.Location = new Point(980, 80);
            this.Controls.Add(colorName);
            greenScroll = new FilterController(980, 110, Color.FromArgb(103, 184, 109), "G", 128, this);
                greenScroll.scroll.MouseUp += Sc_MouseUp; 
            redScroll = new FilterController(980, 140, Color.FromArgb(142, 50, 50), "R", 128, this);
                redScroll.scroll.MouseUp += Sc_MouseUp1;
            blueScroll = new FilterController(980, 170, Color.FromArgb(13, 120, 200), "B", 128, this);
                blueScroll.scroll.MouseUp += Scroll_MouseUp;

            //Creating brightness block
            LabelNameForBlock brightness = new LabelNameForBlock("Brightness");
            brightness.Location = new Point(980, 220);
            this.Controls.Add(brightness);
            brightnessScroll = new FilterController(980, 240, Color.FromArgb(144, 16, 45), "Bright", 128, this);
            brightnessScroll.scroll.MouseUp += Scroll_MouseUp1;

            //Creating PixelizationBlock
            LabelNameForBlock pixelization = new LabelNameForBlock("Pixelization");
            pixelization.Location = new Point(980, 280);
            this.Controls.Add(pixelization);
            pixelScroll = new FilterController(980, 300, Color.FromArgb(225, 166, 41), "P", 0, this);
            pixelScroll.scroll.MouseUp += Scroll_MouseUp2;

            //Creating OpacityBlock
            LabelNameForBlock opacity = new LabelNameForBlock("Opacity");
            opacity.Location = new Point(980, 340);
            this.Controls.Add(opacity);
            opacityScroll = new FilterController(980, 360, Color.FromArgb(75, 50, 70), "O", 255, this);
            opacityScroll.scroll.MouseUp += Scroll_MouseUp3;

            //Creating BlackAndWhiteBlock
            LabelNameForBlock blackWhite = new LabelNameForBlock("Black-White");
            blackWhite.Location = new Point(980, 400);
            this.Controls.Add(blackWhite);
            blackWhiteScroll = new FilterController(980, 420, Color.FromArgb(70, 190, 70), "w", 0, this);
            blackWhiteScroll.scroll.MouseUp += Scroll_MouseUp4;

            //Creating SegmentationBlock
            LabelNameForBlock segmentation = new LabelNameForBlock("Segmentation2D");
            segmentation.Location = new Point(980, 460);
            this.Controls.Add(segmentation);
            segmentation2DScroll = new FilterController(980, 480, Color.FromArgb(41, 144, 225), "S", 0, this);
            segmentation2DScroll.scroll.MouseUp += Scroll_MouseUp5;

            //To inverse button
            inverseButton = new SuperButtonForTools(Color.FromArgb(160, 75, 255), Color.White, "Inverse");
            inverseButton.Location = new Point(865, 72);
            inverseButton.MouseClick += inverseButton_MouseClick;
            this.Controls.Add(inverseButton);

            //BlackAndWhite button
            //blackWhiteButton = new SuperButton(Color.FromArgb(255, 75, 75), Color.White, "B/W");
            //blackWhiteButton.Location = new Point(865, 102);
            //blackWhiteButton.MouseClick += Sb_MouseClick;
            //this.Controls.Add(blackWhiteButton);
            
            //Segmentation button 
            segmentationButton = new SuperButton(Color.FromArgb(255, 75, 75), Color.White, "Segmentation");
            segmentationButton.Location = new Point(865, 102);
            segmentationButton.MouseClick += SegmentationButton_MouseClick;
            this.Controls.Add(segmentationButton);


            //ActiveArea Button
            activeAreaButton = new SuperButtonForTools(Color.FromArgb(255, 75, 75), Color.White, "Selection");
            activeAreaButton.Location = new Point(865, 132);
            activeAreaButton.MouseClick += ActiveAreaButton_MouseClick; ;
            this.Controls.Add(activeAreaButton);
            activeArea = new ActiveArea(pb);

            //NoiseRemove Button
            smoothButton = new SuperButton(Color.FromArgb(255, 75, 75), Color.White, "Smoothing");
            smoothButton.Location = new Point(865, 162);
            smoothButton.MouseClick += Smooth_MouseClick;
            this.Controls.Add(smoothButton);

            //Monochrome
            monoChrome = new SuperButton(Color.FromArgb(255, 75, 75), Color.White, "Monochrome");
            monoChrome.Location = new Point(865, 192);
            monoChrome.MouseClick += MonoChrome_MouseClick; ;
            this.Controls.Add(monoChrome);

  

        }

     





        private void Scroll_MouseUp5(object sender, MouseEventArgs e)
        {
            TwoDimTools segmantetion2D = new TwoDimTools(pb.Image.Width, pb.Image.Height);
            Bitmap b = new Bitmap(pb.Image);
            pb.Image = segmantetion2D.s2d(b, segmentation2DScroll.scroll.value/2);
            segmantetion2D = null;
        }


        private void MonoChrome_MouseClick(object sender, MouseEventArgs e)
        {
            if (activeArea.enable)
                pb.Image = fm.makeMonochrome(activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
            else
                pb.Image = fm.makeMonochrome(0, 0, pb.Image.Width, pb.Image.Height);
        }

        private void Smooth_MouseClick(object sender, MouseEventArgs e)
        {
            Bitmap b = new Bitmap(pb.Image);
            b = medianFilter.MedFilter(b, 8);
            pb.Image = b;
            fm.imagesSet.toImage(b);
        }

        private void Scroll_MouseUp4(object sender, MouseEventArgs e)
        {
            if (activeArea.enable)
                pb.Image = fm.makeBlackAndWhiteScroll(blackWhiteScroll.scroll.value / 2.56, activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
            else
                pb.Image = fm.makeBlackAndWhiteScroll(blackWhiteScroll.scroll.value / 2.56, 0, 0, pb.Image.Width, pb.Image.Height);
        }

        private void ActiveAreaButton_MouseClick(object sender, MouseEventArgs e)
        {
            activeArea.enable =  !activeArea.enable;           
        }

        private void SegmentationButton_MouseClick(object sender, MouseEventArgs e)
        {
            ds = new DanilSegmentation(pb);
            ds.deepSearch(6, 0, 0);
            pb.Image = ds.makeSegmentation();
        }

        private void inverseButton_MouseClick(object sender, MouseEventArgs e)
        {
            if (activeArea.enable)
                pb.Image = fm.Inverse(activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
            else
                pb.Image = fm.Inverse(0, 0, pb.Image.Width, pb.Image.Height);
        }

        /*private void Sb_MouseClick(object sender, MouseEventArgs e)
        {
            if (blackWhiteButton.getClickCounter() % 2 != 11)
                if (activeArea.enable)
                    pb.Image = fm.makeBlackAndWhite(activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
                else
                    pb.Image = fm.makeBlackAndWhite(0, 0, pb.Image.Width, pb.Image.Height);
            else
            {//Parshukov Danil
                pb.Image = fm.imagesSet.forBW;
                fm.imagesSet.toImageExeptOriginal(fm.imagesSet.forBW);
            }
        }*/


        //When image is changed
        private void Pb_LoadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            Bitmap b = new Bitmap(pb.Image);
            activeArea.changeMainImage(pb);
            fm.imagesSet.toImage(b);
        }

        //OPACITY SCROLL
        private void Scroll_MouseUp3(object sender, MouseEventArgs e)
        {
            if (activeArea.enable)
                pb.Image = fm.changeOpacity((opacityScroll.scroll.value / 2 - 128) * 2, activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
            else
                pb.Image = fm.changeOpacity((opacityScroll.scroll.value / 2 - 128) * 2, 0, 0, pb.Image.Width, pb.Image.Height);
                
        }

        //PIXELIZATION
        private void Scroll_MouseUp2(object sender, MouseEventArgs e)
        {
            if (activeArea.enable)
                pb.Image = fm.makePixelImage((pixelScroll.scroll.value), activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
            else
                pb.Image = fm.makePixelImage((pixelScroll.scroll.value), 0, 0, pb.Image.Width, pb.Image.Height);
        }

        //BRIGHTNESS
        private void Scroll_MouseUp1(object sender, MouseEventArgs e)
        {
            if(activeArea.enable)
                pb.Image = fm.changeBrightness((brightnessScroll.scroll.value - 128) * 2, activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
            else
                pb.Image = fm.changeBrightness((brightnessScroll.scroll.value - 128) * 2, 0, 0, pb.Image.Width, pb.Image.Height);
        }

        //FOR BLUE COLOR
        private void Scroll_MouseUp(object sender, MouseEventArgs e)
        {
            if (activeArea.enable)
                pb.Image = fm.changeBlue((blueScroll.scroll.value - 128) * 2, activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
            else
                pb.Image = fm.changeBlue((blueScroll.scroll.value - 128) * 2, 0, 0, pb.Image.Width, pb.Image.Height);
        }

        //FOR GREEN COLOR
        private void Sc_MouseUp(object sender, MouseEventArgs e)
        {
            if (activeArea.enable)
                pb.Image = fm.changeGreen((greenScroll.scroll.value - 128) * 2, activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
            else
                pb.Image = fm.changeGreen((greenScroll.scroll.value - 128) * 2, 0, 0, pb.Image.Width, pb.Image.Height);
        }

        //FOR RED COLOR
        private void Sc_MouseUp1(object sender, MouseEventArgs e)
        {
            if (activeArea.enable)
                pb.Image = fm.changeRed((redScroll.scroll.value - 128) * 2, activeArea.originalX, activeArea.originalY, activeArea.originalWidth, activeArea.originalHeihgt);
            else
                pb.Image = fm.changeRed((redScroll.scroll.value - 128) * 2, 0, 0, pb.Image.Width, pb.Image.Height);
        }

    }
}
